# Static Website Hosting


## S3 Bucket Hosting


### Set Up AWS S3

* Create a bucket. Make it publicly readable.
* Enable bucket hosting. Set the CORS rule.
* Set bucket policy. 

Here's an example:

    {
      "Version":"2012-10-17",
      "Statement":[{
	    "Sid":"PublicReadGetObject",
        "Effect":"Allow",
	    "Principal": "*",
        "Action":["s3:GetObject"],
        "Resource":["arn:aws:s3:::bucket-name/*"]
      }]
    }


For a SPA app (with a "single page", index.html),
routing cannot be correctly handled by S3 (which is really a basic Web server).
We need to set the "error document" to "index.html" to be able to handle URLs not directed to the root ("/").
(That is, in the bucket's hosting property, set both index and error documents to "index.html".)

This works more or less, but you'll get "404" status code instead of "200" for the non-root URL requests.
There is no workaround for this.
You'll need to use CloudFront if you don't want such a behavior.
Or, better yet, you may want to use a more full-blown website hosting service, or at least a better static site hosting service
like Netlify or Surge.sh, etc.
S3 (even with CloudFront hack) was never designed for serving modern SPA apps with client-side routing.



### Build and Deploy

Build your app.


Upload dist files.



### Verify

http://bucket-name.s3-website-region-name.amazonaws.com

http://bucket-name.s3-website-region-name.amazonaws.com/bogus-page




## Cloudfront




## Custom Domain Setup




## Amazon Certificate Manager






_tbd_



## References


* [Hosting a Static Website on Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
* [Configuring a Bucket for Website Hosting](https://docs.aws.amazon.com/AmazonS3/latest/dev/HowDoIWebsiteConfiguration.html)
* [Permissions Required for Website Access](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteAccessPermissionsReqd.html)
* [Configuring a Webpage Redirect](https://docs.aws.amazon.com/AmazonS3/latest/dev/how-to-page-redirect.html)
* [Example: Setting up a Static Website Using a Custom Domain](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html)
* [Create a CloudFront Web Distribution](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/GettingStarted.html#GettingStartedCreateDistribution)
* []()
* []()
* []()

